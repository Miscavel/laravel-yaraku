<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use App\Author;
use App\Book;

class BooksTest extends TestCase
{
	use RefreshDatabase;
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    /** @test*/
    public function create_book()
    {
        //User is able to create book by passing ['author_id', 'title'] as parameter

        //Create a fake author to get the required author_id
        $author = factory('App\Author')->create();

        //Book's attributes
        $param = ['title' => $this->faker->sentence];

        //Create a book with the given attributes under the created author's author_id
        //books() refers to eloquent relationship hasMany() that can be found in Author model
        $book = $author->books()->create($param);

        //Check if database has an entry with the given attributes
        $this->assertDatabaseHas('books', $param);
    }

    /** @test*/
    public function check_author_book_combination_unique_on_creation()
    {
		/**
		 * Combination of ['author_id', 'title'] has to be unique
		 *
		 * For example, the following records are NOT allowed :
		 * id           author_id    title
		 * 1            1            My Book Title
		 * 2            1            My Book Title
		 *
		 * However, the following records are allowed :
		 * id           author_id    title
		 * 1            1            My Book Title
		 * 2            2            My Book Title
		 */ 

		//Create 1st fake author
		$author_1 = factory('App\Author')->create();

		//Create 2nd fake author
		$author_2 = factory('App\Author')->create();

		//Book's attributes
		$param = ['title' => $this->faker->sentence];

		//Assert status
		$status = true;

		//1st author creates a book with the given attributes
		$author_1->books()->create($param);

		//CASE 1: The same author tries to register the same title twice
		try
		{
			//1st author creates another book with the same title
			$author_1->books()->create($param);

			//If error not caught, means database fails in rejecting registration of duplicate combination
			$status *= false;
		}
		catch(QueryException $exception)
		{
			//If error caught, means database successfully rejects registration of duplicate combination
		}

		//CASE 2: 2 different authors try to register the same title
		try
		{
			//2nd author creates a book with the same title
			$author_2->books()->create($param);

			//If error not caught, means database succeeds in allowing the registration of a unique combination
		}
		catch(QueryException $exception)
		{
			//If error caught, means database incorrectly rejects the registration of a unique combination
			$status *= false;
		}

		$this->assertTrue($status);
    }

    /** @test*/
    public function update_book_title()
    {
    	//User is able to edit a book's title

    	//Create fake author
		$author = factory('App\Author')->create();

		//Book's original attributes
		$param = ['title' => $this->faker->sentence];

		//Book's attributes after update
		$param_edit = ['title' => 'Edited Title'];

		//Create a book with the original title
		$author->books()->create($param);

		//Change the book's title into the new one
		$author->books()->first()->update($param_edit);

		//Check if database has an entry with the new attributes
        $this->assertDatabaseHas('books', $param_edit);
    }

    /** @test*/
    public function update_book_author()
    {
    	//User is able to edit a book's author_id

    	//Create 1st fake author
		$author_1 = factory('App\Author')->create();

		//Create 2nd fake author
		$author_2 = factory('App\Author')->create();

		//Book's attributes
		$param = ['title' => $this->faker->sentence];

		//1st author creates a book with the given attributes
		$book = $author_1->books()->create($param);

		//Update the book's author_id into the 2nd author's id
		$book->update(['author_id' => $author_2->id]);

		//Check if database has an entry with the new combination
        $this->assertDatabaseHas('books', ['author_id' => $author_2->id, 'title' => $param['title']]);
    }

    /** @test*/
    public function update_book_title_author()
    {
    	//User is able to edit a book's title and author_id

    	//Create 1st fake author
		$author_1 = factory('App\Author')->create();

		//Create 2nd fake author
		$author_2 = factory('App\Author')->create();

		//Book's original attributes
		$param = ['title' => $this->faker->sentence];

		//Book's attributes after update
		$param_edit = ['title' => 'Edited Title'];

		//Create a book with the original title and author
		$book = $author_1->books()->create($param);

		//Update the book's title into the new title, and author_id into the 2nd author's id
		$book->update(['author_id' => $author_2->id, 'title' => $param_edit['title']]);

		//Check if database has an entry with the new combination
        $this->assertDatabaseHas('books', ['author_id' => $author_2->id, 'title' => $param_edit['title']]);
    }

    /** @test*/
    public function check_author_book_combination_unique_on_update()
    {
    	/**
		 * Combination of ['author_id', 'title'] has to be unique
		 *
		 * For example, the following records are NOT allowed :
		 * id           author_id    title
		 * 1            1            My Book Title
		 * 2            1            My Book Title
		 *
		 * However, the following records are allowed :
		 * id           author_id    title
		 * 1            1            My Book Title
		 * 2            2            My Book Title
		 */ 

		//Create 1st fake author
		$author_1 = factory('App\Author')->create();

		//Create 2nd fake author
		$author_2 = factory('App\Author')->create();

		//Create 3rd fake author
		$author_3 = factory('App\Author')->create();

		//1st Book's attributes
		$param_1 = ['title' => $this->faker->sentence];

		//2nd Book's attributes
		$param_2 = ['title' => '2nd Book'];

		//Assert status
		$status = true;

		//1st author creates book_1 with the 1st book's attributes
		$book_1 = $author_1->books()->create($param_1);

		//2nd author creates book_2 with the 1st book's attributes
		$book_2 = $author_2->books()->create($param_1);

		//3rd author creates book_3 with the 2nd book's attributes
		$book_3 = $author_3->books()->create($param_2);

		//CASE 1: Try to change a book's author_id into another author that already has a book with the same title
		try
		{
			//Change book_2's author_id into author_1's id (who already has book_1 with the same title)
			$book_2->update(['author_id' => $author_1->id]);

			//If error not caught, means database fails in rejecting update of duplicate combination
			$status *= false;
		}
		catch(QueryException $exception)
		{
			//If error caught, means database successfully rejects update of duplicate combination
		}

		//CASE 2: Try to change a book's author_id into another author that DOES NOT have a book with the same title
		try
		{
			//Change book_2's author_id into author_3's id (who has book_3 with different title)
			$book_2->update(['author_id' => $author_3->id]);

			//If error not caught, means database succeeds in allowing the update of a unique combination
		}
		catch(QueryException $exception)
		{
			//If error caught, means database incorrectly rejects the update of a unique combination
			$status *= false;
		}

		$this->assertTrue($status);
    }

    /** @test*/
    public function delete_book()
    {
    	//A test to check that a delete call removes that book's entry from the database

    	 //Create a fake author to get the required author_id
        $author = factory('App\Author')->create();

        //Book's attributes
        $param = ['title' => $this->faker->sentence];

        //Create a book with the given attributes under the created author's author_id
        $book = $author->books()->create($param);

        //Delete book
        $book->delete();

        //Verify that the book's entry is not found in the database
        $this->assertDatabaseMissing('books', ['author_id' => $author->id, 'title' => $param['title']]);
    }

    /** @test*/
    public function delete_book_on_author_delete()
    {
    	//A test to check that a book is deleted when its author is deleted

    	 //Create a fake author to get the required author_id
        $author = factory('App\Author')->create();

        //Book's attributes
        $param = ['title' => $this->faker->sentence];

        //Create a book with the given attributes under the created author's author_id
        $book = $author->books()->create($param);

        //Delete author
        $author->delete();

        //Verify that the book's entry is not found in the database
        $this->assertDatabaseMissing('books', ['author_id' => $author->id, 'title' => $param['title']]);
    }
}
