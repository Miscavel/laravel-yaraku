<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorsRouteTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test*/
    public function index_route_test()
    {
        $response = $this->get(route('authors.index'));

        $response->assertStatus(200);
    }

    /** @test*/
    public function create_route_test()
    {
        $response = $this->get(route('authors.create'));

        $response->assertStatus(200);
    }

    /** @test*/
    public function store_complete_test()
    {
        //Test a store request with complete field
        $response = $this->post(route('authors.store'), [
            'name' => $this->faker->name()
        ]);

        //Check if store request is successful
        $response->assertSessionHasNoErrors();
    }

    /** @test*/
    public function store_missing_name_test()
    {
        //Test a store request with empty name field
        $response = $this->post(route('authors.store'), []);

        //Check if store request has name validation error
        $response->assertSessionHasErrors(['name']);
    }

    /** @test*/
    public function store_name_less_than_5_test()
    {
        //Test a store request with name length less than 5
        $response = $this->post(route('authors.store'), [
            'name' => 'Misc'
        ]);

        //Check if store request has name validation error
        $response->assertSessionHasErrors(['name']);
    }

    /** @test*/
    public function store_non_unique_name_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Test a store request with existing name
        $response = $this->post(route('authors.store'), [
            'name' => $author->name
        ]);

        //Check if store request has name validation error
        $response->assertSessionHasErrors(['name']);
    }

    /** @test*/
    public function show_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Test a show request on an existing author_id
        $response = $this->call('GET', '/authors/' . $author->id);

        $response->assertStatus(200);
    }

    /** @test*/
    public function show_non_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Delete author
        $author->delete();

        //Test a show request on a non-existing author_id
        $response = $this->call('GET', '/authors/' . $author->id);

        $response->assertStatus(404);
    }

    /** @test*/
    public function edit_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Test an edit request on an existing author_id
        $response = $this->call('GET', '/authors/' . $author->id . '/edit');

        $response->assertStatus(200);
    }

    /** @test*/
    public function edit_non_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Delete author
        $author->delete();

        //Test an edit request on a non-existing author_id
        $response = $this->call('GET', '/authors/' . $author->id . '/edit');

        $response->assertStatus(404);
    }

    /** @test*/
    public function update_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Create a new name
        $new_name = $this->faker->name();

        //Test a name update request on an existing author_id
        $response = $this->call('PATCH', '/authors/' . $author->id, [
            'name' => $new_name
        ]);

        //Check if the entry with the updated name exists in the database
        $this->assertDatabaseHas('authors', ['name' => $new_name]);
    }

    /** @test*/
    public function update_non_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Create a new name
        $new_name = $this->faker->name();

        //Delete author
        $author->delete();

        //Test a name update request on a non-existing author_id
        $response = $this->call('PATCH', '/authors/' . $author->id, [
            'name' => $new_name
        ]);

        $response->assertStatus(404);
    }

    /** @test*/
    public function delete_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Test delete request on an existing author_id
        $response = $this->call('DELETE', '/authors/' . $author->id, []);

        //Confirm that the author has been deleted from the database
        $this->assertDatabaseMissing('authors', $author->getAttributes());
    }

    /** @test*/
    public function delete_non_existing_author_test()
    {
        //Create a fake author
        $author = factory('App\Author')->create();

        //Delete author
        $author->delete();

        //Test delete request on a non-existing author_id
        $response = $this->call('DELETE', '/authors/' . $author->id, []);

        $response->assertStatus(404);
    }
}
