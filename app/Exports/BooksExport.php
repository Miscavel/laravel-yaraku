<?php

namespace App\Exports;

use App\Book;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\XMLParser;

class BooksExport implements FromCollection, WithHeadings
{
	public function __construct($columns)
    {
        $this->columns = $columns;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $books = Book::all()->map(function ($book) {
            $book->assignAuthorName();
            return $book->only($this->columns);
        });
        return $books;
    }

    public function headings(): array
    {
        return $this->columns;
    }

    public function toXML()
    {
        $xmlParser = new XMLParser();
        return $xmlParser->parse($this->collection()->toArray(), 'books', 'book');
    }
}
