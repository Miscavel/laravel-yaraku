<?php

namespace App;

use XMLWriter;

class XMLParser
{
	public function parse($array, $root, $segment)
	{
		$xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0');
        $xml->startElement($root);
        foreach ($array as $item) {
            $xml->startElement($segment);
            foreach ($item as $key => $value)
            {
                $xml->writeElement($key, $value);
            }
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();

        $content = $xml->outputMemory();
        $xml->flush();
        
        return $content;
	}
}