<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
	protected $fillable = ['name'];
	
    public function books()
    {
        return $this->hasMany(Book::class);
    }

    /**
     * Used to return columns that are viewable in tables and forms (for AuthorController:create() and
     * AuthorController:edit())
     */
    public function editableAttributes()
    {
        return $this->only($this->fillable);
    }

    /**
     * Used to return columns that are viewable in tables and forms (for AuthorController:index() and
     * AuthorController:show())
     */
    public function showableAttributes()
    {
        return $this->only(['id', 'name']);
    }
}
