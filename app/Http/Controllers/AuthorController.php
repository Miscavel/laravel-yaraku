<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use DB;
use App\Exports\AuthorsExport;
use Maatwebsite\Excel\Facades\Excel;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    const DIRECTORY = 'authors';
    const LIST_COLUMNS = ['id', 'name']; //Columns to return in fetch API
 
    public function index(Request $request)
    {
        $authors = $this->getFilteredResult($request);
        return view(self::DIRECTORY . '.index', [
            'items' => $authors,

            /**
             *
             * Option is used to give additional column for view / edit / delete
             * For the 1st option, it translates to a HEAD request to : /authors/{{ $author->id }}
             * For the 2nd option, it translates to a HEAD request to : /authors/{{ $author->id }}/edit
             * For the 3rd option, it translates to a DELETE request to : /authors//{{ $author->id }}
             *
             */

            'options' => [
                ['name' => 'View', 'icon' => 'fa-eye', 'method_form' => 'HEAD', 'method' => '', 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '', 'type' => 'link'],
                ['name' => 'Edit', 'icon' => 'fa-pencil', 'method_form' => 'HEAD', 'method' => '', 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '/edit', 'type' => 'link'],
                ['name' => 'Delete', 'icon' => 'fa-trash', 'method_form' => 'POST', 'method' => method_field('DELETE'), 'action_prefix' => '/' . self::DIRECTORY, 'action_suffix' => '', 'type' => 'button']
            ],

            /**
             *
             * Filter array is used for the filter.blade.template
             * search = contains an array of searchable columns
             * sort = contains an array of sortable columns
             * url = the url of index() for search GET requests and create button redirect
             *
             */

            'filter' => [
                'search' => ['author'],
                'sort' => ['author'],
                'url' => '/' . self::DIRECTORY
            ]
        ]);
    }

    /**
     * Get a list of authors based on the search parameters
     */
    public function getFilteredResult(Request $request)
    {
        $authors = Author::where('id', '>', 0);
        if ($request->search_field === 'author')
        {

            /**
             * Get a list of authors containing the input string
             */

            $authors = $this->queryLike($request->search_value);
        }

        if ($request->action === 'asc' || $request->action === 'desc')
        {

            /**
             * Sorth author names based on the given option ('asc' or 'desc')
             */

            $orderColumn = 'authors.name';
            $authors->orderBy($orderColumn, $request->action)
                ->select('authors.*');
        }
        return $authors->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::DIRECTORY . '.create', [
            'title' => 'Create Author', //Form title
            'items' => Author::make()->editableAttributes(), //Fetch editable items from empty Author model
            'readonly' => false, //Input fields are editable
            'method_form' => 'POST', //Upon pressing confirm, send a POST request to $confirmAction
            'method' => '', //No custom method is required
            'confirmAction' => '/' . self::DIRECTORY, //Upon pressing confirm, send a $method_form request to /authors
            'confirmText' => 'Create', //Label confirm button as 'Create'
            'cancelAction' => '/' . self::DIRECTORY, //Upon pressing cancel, redirect user back to /authors
            'cancelText' => 'Cancel' //Label cancel button as 'Cancel'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Author::create($this->validateAuthor()); //Validate request data before creating
        return redirect('/' . self::DIRECTORY); //Redirects to /authors after successful creation
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        return view(self::DIRECTORY . '.show', [
            'title' => 'Show Author', //Form title
            'items' => $author->showableAttributes(), //Fetch showable attributes from searched author
            'readonly' => true, //Input fields are not editable
            'method_form' => 'HEAD', //Upon pressing confirm, send a HEAD request to $confirmAction
            'method' => '', //No custom method is required
            'confirmAction' => '/' . self::DIRECTORY . '/' . $author->id . '/edit', //Upon pressing confirm, send a $method_form request to /authors/{{ $author->id }}/edit
            'confirmText' => 'Go to Edit', //Label confirm button as 'Go to Edit'
            'cancelAction' => '/' . self::DIRECTORY, //Upon pressing cancel, redirect user back to /authors
            'cancelText' => 'Back to Authors' //Label cancel button as 'Back to Authors'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        return view(self::DIRECTORY . '.edit', [
            'title' => 'Edit Author', //Form title
            'items' => $author->editableAttributes(), //Fetch editable attributes from searched author
            'readonly' => false, //Input fields are editable
            'method_form' => 'POST', //Upon pressing confirm, send a POST request to $confirmAction
            'method' => method_field('PATCH'), //Use laravel custom method PATCH
            'confirmAction' => '/' . self::DIRECTORY . '/' . $author->id, //Upon pressing confirm, send a $method_form request to /authors/{{ $author->id }}
            'confirmText' => 'Submit', //Label confirm button as 'Submit'
            'cancelAction' => '/' . self::DIRECTORY . '/' . $author->id, //Upon pressing cancel, redirect user back to /authors/{{ $author->id }}
            'cancelText' => 'Cancel' //Label cancel button as 'Cancel'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        $author->update($this->validateAuthor()); //Validate request data before updating
        return redirect('/' . self::DIRECTORY . '/' . $author->id); //Redirects to /authors/{{ $author->id }} after successful update
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        $author->delete(); //Delete the selected author
        return redirect('/' . self::DIRECTORY); //Redirects to /authors after successful delete
    }

    /**
     * Validation of author data for create and update
     */
    public function validateAuthor()
    {
        return request()->validate([
            'name' => ['required', 'min:5', 'unique:authors']
        ]);
    }

    public function queryLike($name)
    {
        return Author::where(DB::raw('LOWER(name)'), 'LIKE', '%' . strtolower($name) . '%');
    }

    /**
     * API to return all authors with columns according to self::LIST_COLUMNS in JSON format
     */
    public function list()
    {
        return Author::all(self::LIST_COLUMNS)->toJson();
    }

    /**
     * API to return authors that contain the given name with columns according to self::LIST_COLUMNS in JSON format
     */
    public function listContains($name)
    {
        return $this->queryLike($name)->get(self::LIST_COLUMNS)->toJson();
    }

    public function exportToCsv()
    {
        return Excel::download(new AuthorsExport(['name']), 'authors.csv');
    }

    public function exportToXml()
    {   
        $exporter = new AuthorsExport(['name']);
        $xml = $exporter->toXML();
        return response($xml)
            ->header('Content-Type', 'text/xml')
            ->header('Content-Disposition', 'attachment; filename="authors.xml"');
    }
}
