warning: LF will be replaced by CRLF in app/Http/Controllers/BookController.php.
The file will have its original line endings in your working directory.
[1mdiff --git a/app/Http/Controllers/BookController.php b/app/Http/Controllers/BookController.php[m
[1mindex 2627734..d6ee5f8 100644[m
[1m--- a/app/Http/Controllers/BookController.php[m
[1m+++ b/app/Http/Controllers/BookController.php[m
[36m@@ -44,7 +44,7 @@[m [mclass BookController extends Controller[m
              * url = the url of index() for search GET requests and create button redirect[m
              *[m
              */[m
[31m-            [m
[32m+[m
             'filter' => [[m
                 'search' => ['author', 'book'],[m
                 'sort' => ['author', 'book'],[m
[36m@@ -53,6 +53,61 @@[m [mclass BookController extends Controller[m
         ]);[m
     }[m
 [m
[32m+[m
[32m+[m[32m    /**[m
[32m+[m[32m     * Get a list of books based on the search parameters[m
[32m+[m[32m     */[m
[32m+[m[32m    public function getFilteredResult(Request $request)[m
[32m+[m[32m    {[m
[32m+[m[32m        $books = Book::where('id', '>', 0);[m
[32m+[m[32m        if ($request->search_field === 'author')[m
[32m+[m[32m        {[m
[32m+[m
[32m+[m[32m            /**[m
[32m+[m[32m             * If search by author, first fetch a list of authors containing the given name[m
[32m+[m[32m             */[m
[32m+[m
[32m+[m[32m            $apiRequest = Request::create('/authors/fetch/' . $request->search_value, 'GET');[m
[32m+[m[32m            $response = \Route::dispatch($apiRequest);[m
[32m+[m[32m            $json = json_decode($response->getOriginalContent());[m
[32m+[m[32m            $authors = [];[m
[32m+[m[32m            foreach ($json as $item)[m
[32m+[m[32m            {[m
[32m+[m[32m                array_push($authors, $item->id);[m
[32m+[m[32m            }[m
[32m+[m
[32m+[m[32m            /**[m
[32m+[m[32m             * Then, get a list of books that contain those authors' author_id[m
[32m+[m[32m             */[m
[32m+[m
[32m+[m[32m            $books = Book::whereIn('author_id', $authors);[m
[32m+[m[32m        }[m
[32m+[m[32m        else if ($request->search_field === 'book')[m
[32m+[m[32m        {[m
[32m+[m
[32m+[m[32m            /**[m
[32m+[m[32m             * If search by book, then look for books which title contains the given string[m
[32m+[m[32m             */[m
[32m+[m
[32m+[m[32m            $books = Book::where(DB::raw('LOWER(title)'), 'LIKE', '%' . strtolower($request->search_value) . '%');[m
[32m+[m[32m        }[m
[32m+[m
[32m+[m[32m        if ($request->action === 'asc' || $request->action === 'desc')[m
[32m+[m[32m        {[m
[32m+[m
[32m+[m[32m            /**[m
[32m+[m[32m             * Sort the current search result based on the given column, 'authors.name' or 'books.title'[m
[32m+[m[32m             * $request->action determines the direction of sorting, 'asc' or 'desc'[m
[32m+[m[32m             */[m
[32m+[m
[32m+[m[32m            $orderColumn = ($request->sort_field === 'author') ? 'authors.name' : 'books.title';[m
[32m+[m[32m            $books->join('authors', 'authors.id', '=', 'books.author_id')[m
[32m+[m[32m                ->orderBy($orderColumn, $request->action)[m
[32m+[m[32m                ->select('books.*');[m
[32m+[m[32m        }[m
[32m+[m[32m        return $books->get();[m
[32m+[m[32m    }[m
[32m+[m
     /**[m
      * Show the form for creating a new resource.[m
      *[m
