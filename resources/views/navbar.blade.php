<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="https://gitlab.com/Miscavel/laravel-yaraku">
      <img src="{{ url('/images/gitlab.png') }}" width="112" height="28">
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbar">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

	<div id="navbar" class="navbar-menu">
		<div class="navbar-start">
			<a class="navbar-item" id="navbar-books" href="/books">
				Books
			</a>

			<a class="navbar-item" id="navbar-authors" href="/authors">
				Authors
			</a>

			<div class="navbar-item has-dropdown is-hoverable">
				<a class="navbar-link">
				  Export CSV
				</a>

				<div class="navbar-dropdown">
					<a class="navbar-item" href="/authors/export/csv">
						Authors
					</a>
					<hr class="navbar-divider">
					<a class="navbar-item" href="/books/export/csv/0">
						Titles
					</a>
					<hr class="navbar-divider">
					<a class="navbar-item" href="/books/export/csv/1">
						Authors & Titles
					</a>
				</div>
			</div>

			<div class="navbar-item has-dropdown is-hoverable">
				<a class="navbar-link">
				  Export XML
				</a>

				<div class="navbar-dropdown">
					<a class="navbar-item" href="/authors/export/xml">
						Authors
					</a>
					<hr class="navbar-divider">
					<a class="navbar-item" href="/books/export/xml/0">
						Titles
					</a>
					<hr class="navbar-divider">
					<a class="navbar-item" href="/books/export/xml/1">
						Authors & Titles
					</a>
				</div>
			</div>
		</div>

		<div class="navbar-end">
			<div class="navbar-item">
				<div class="buttons">

				</div>
			</div>
		</div>
  </div>
</nav>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', () => {
		// Get all "navbar-burger" elements
		const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {
			// Add a click event on each of them
			$navbarBurgers.forEach( el => {
				el.addEventListener('click', () => {
					// Get the target from the "data-target" attribute
					const target = el.dataset.target;
					const $target = document.getElementById(target);

					// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
					el.classList.toggle('is-active');
					$target.classList.toggle('is-active');
				});
			});
		}
	});
</script>