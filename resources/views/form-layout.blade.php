
{{-- 
 *
 * General form format for create, update, delete
 *
 * $method_form = general method of the form (GET or POST)
 * $confirmAction = target url when the confirm button is pressed 
 *
 --}}

<form method="{{ $method_form }}" action="{{ $confirmAction }}" class="field">
   
    {{-- 
     *
     * $method = custom method for laravel's RESTful routing (DELETE, PATCH)
     * $title = title of the form (e.g. Create Author)
     *
     --}}

    @if ($method_form === 'POST')
        @csrf
    @endif
    {{ $method }}
    <h1 class="title">{{ $title }}</h1>

        {{-- 
         *
         * @yield('additional-input') = for additional input field if ever needed
         * $items = array of data to be rendered [$key => 'column_name', $value => 'data']
         * $readonly = to determine if the input field is editable
         *
         --}}

        @yield('additional-input')
        @foreach ($items as $key => $value)
            <fieldset class="field" {{ $readonly ? 'disabled' : '' }}>
                <label class="label" for="{{ $key }}">{{ ucfirst($key) }}</label>
                <div class="control">
                    <input type="text" class="input {{ $errors->has($key) ? 'is-danger' : '' }}" name="{{ $key }}" value="{{ $errors->has($key) ? old($key) : $value }}" />
                </div>
            </fieldset>
        @endforeach

    {{-- 
     *
     * $confirmText = text on confirm button
     * $cancelText = text on cancel button
     *
     --}}

    <div class="field is-grouped">
        <div class="control">
            <button type="submit" class="button is-link">{{ $confirmText }}</button>
        </div>
</form>
<form action="{{ $cancelAction }}">
        <div class="control">
            <button type="submit" class="button is-light">{{ $cancelText }}</button>
        </div>
    </div>
</form>

@include('errors')