@extends('books.layout')

@section('content')
	<div class="box">
		
		{{--
		 *
		 * Displays search and sort filters with parameters according to the value of $filter set in the controller
		 * Also includes a create button to redirect to 'create' route
		 *
		 --}}

		@include('filter')

		{{--
		 *
		 * Displays books array passed though as $items with full-table-layout template
		 *
		 --}}

		@include('full-table-layout')
	</div>
@endsection