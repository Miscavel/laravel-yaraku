
{{-- 
 *
 * Filter for search, sort, and create buttons
 * Url for the following actions depends on the value of $filter['url'] passed by the controller
 *
 --}}

<form class="field" method="GET" action="{{ $filter['url'] }}">
	<div class="field is-horizontal">
		<div class="field-body">
			<div class="field is-narrow">
    			<p class="control">
       				<span class="select">

						{{-- 
						 *
						 * Provides an option select for which column to search on (e.g. 'name', 'title')
						 *
						 --}}

          				<select name="search_field">
          					@foreach ($filter['search'] as $item)
                 				<option value="{{ $item }}" {{ ($item === request()->search_field) ? 'selected' : '' }}>{{ ucfirst($item) }}</option>
                 			@endforeach
          				</select>
       				</span>
    			</p>
 			</div>
			<div class="field has-addons is-narrow">
				<p class="control has-icons-left">
					<input type="text" class="input" name="search_value" placeholder="Search.." value="{{ request()->search_value }}" />
					<span class="icon is-small is-left"><i class="fa fa-search"></i></span>
				</p>
				<div class="control">
					<button class="button is-info" name="action" value="search">Search</button>
				</div>
			</div>
			<div class="field has-addons is-narrow">
				<p class="control has-icons-left">
					<span class="select">

						{{-- 
						 *
						 * Provides an option select for which column to sort on (e.g. 'name', 'title')
						 *
						 --}}

          				<select name="sort_field">
             				@foreach ($filter['sort'] as $item)
                 				<option value="{{ $item }}" {{ ($item === request()->sort_field) ? 'selected' : '' }}>{{ ucfirst($item) }}</option>
                 			@endforeach
          				</select>
       				</span>
					<span class="icon is-small is-left"><i class="fa fa-sort"></i></span>
				</p>
				<div class="control">
					<button class="button is-success" name="action" value="asc">
						<i class="fa fa-arrow-up"></i>
					</button>
				</div>
				<div class="control">
					<button class="button is-warning" name="action" value ="desc">
						<i class="fa fa-arrow-down"></i>
					</button>
				</div>
			</div>

			{{-- 
			 *
			 * Button to go to the 'create' route for the respective controller
			 *
			 --}}

			<div class="field is-narrow">
				<p class="control">
					<a class="button is-link" href="{{ $filter['url'] }}/create">Add Entry</a>
				</p>
			</div>
			</div>
	</div>
</form>