<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/authors/fetch', 'AuthorController@list');
Route::get('/authors/fetch/{name}', 'AuthorController@listContains');
Route::get('/authors/export/csv', 'AuthorController@exportToCsv');
Route::get('/authors/export/xml', 'AuthorController@exportToXml');
Route::resource('authors', 'AuthorController');

Route::get('/books/export/csv/{key}', 'BookController@exportToCsv');
Route::get('/books/export/xml/{key}', 'BookController@exportToXml');
Route::resource('books', 'BookController');

Route::get('/', function () {
    return redirect('/books');
});
