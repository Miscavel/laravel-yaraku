# Laravel Yaraku

Repository for Yaraku web-developer task.

The live version of the site can be found in : https://laravel-yaraku.herokuapp.com/

It is a website that allows the user to register Book entries which consists of Author & Title.

Tools :
1. Laravel 5.8 for backend
2. Heroku PostgreSQL for persistent database
3. Heroku for hosting
4. Others: Bulma CSS, jQuery, Font Awesome 4.7.0, Laravel Excel

# User Guide

1. Register Author

![author-index](screenshots/authors-index.png)

Before creating Book entries, there has to be at least one entry in the Authors table.

User may register new authors by clicking on the 'Add Entry' button at the Authors page.

2. Register Book

![books-create](screenshots/books-create.png)

Once the Authors table is populated, user may click on the 'Add Entry' button at the Books page to register a new book.

When registering a new book, the user has to first select the book's author. The user can type in the author field to search for the desired author by his or her name.

For example, typing 'm' would show a dropdown list of authors whose name contains the letter 'm'.

3. Search Entry

![books-search](screenshots/books-search.png)

To search for an entry, the user has to select which column would the search be based on. Afterwards, the user can type in the search bar and press on the search button to find the entries which selected column contains the search keyword.

4. Sort Entry

![books-sort](screenshots/books-sort.png)

The user may sort the entry based on the column selected in the list. Pressing on the 'up' arrow would order the entries in an ASCENDING order of the selected column, while the 'down' arrow in a DESCENDING order of the selected column.